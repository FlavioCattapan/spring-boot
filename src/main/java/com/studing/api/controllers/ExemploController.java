package com.studing.api.controllers;


import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.studing.api.dto.EmpresaDTO;
import com.studing.api.response.Response;

@RestController
@RequestMapping("/api/exemplo")
public class ExemploController {
	
	@GetMapping(value = "/{nome}")
	public String exemplo(@PathVariable("nome") String nome) {
		 return "Olá " + nome;
	}
	
	@PostMapping
	public ResponseEntity<Response<EmpresaDTO>> cadastrar(@Valid @RequestBody EmpresaDTO empresaDTO, BindingResult result){
		Response<EmpresaDTO> response = new Response<EmpresaDTO>();
		response.setData(empresaDTO);
		empresaDTO.setId(15l);
		if(result.hasErrors()) {
			result.getAllErrors().forEach(error -> {
				response.getErrors().add(error.getDefaultMessage());
			});
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
		
	}

}
