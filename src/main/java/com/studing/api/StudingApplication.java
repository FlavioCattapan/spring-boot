package com.studing.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.studing.api.repository.EmpresaRepository;
@SpringBootApplication
public class StudingApplication {
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Value("${paginacao.qtd_por_pagina}")
	private Integer qtdPorPagina;

	public static void main(String[] args) {
		SpringApplication.run(StudingApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
//			System.out.println("Qtd elementos por página : "+qtdPorPagina);
//			
//			String senha1 = SenhaUtils.gerarEncript("123456");
//			
//			String senha2 = SenhaUtils.gerarEncript("123456");
//			
//			System.out.println(senha1);
//			
//			System.out.println(senha2);
//			
//			System.out.println(SenhaUtils.senhaValida("123456", senha1));
//			
//			Empresa empresa = new Empresa();
//			empresa.setRazaoSocial("Kazale IT");
//			empresa.setCnpj("74645215000104");
//			
//			this.empresaRepository.save(empresa);
//
//			List<Empresa> empresas = empresaRepository.findAll();
//			empresas.forEach(System.out::println);
//			
//			Empresa myEmp = new Empresa();
//			myEmp.setId(1l);
//			
//			Optional<Empresa> opEmp = empresaRepository.findById(1l);
//			System.out.println("Empresa por ID: " + opEmp.get());
//			
//			opEmp.get().setRazaoSocial("Kazale IT Web");
//			this.empresaRepository.save(opEmp.get());
//
//			Empresa empresaCnpj = empresaRepository.findByCnpj("74645215000104");
//			System.out.println("Empresa por CNPJ: " + empresaCnpj);
//			
//			
//			this.empresaRepository.delete(myEmp);
//			empresas = empresaRepository.findAll();
//			System.out.println("Empresas: " + empresas.size());
			
		};
		
	}

}
